// demo
var cat1 = {
  name: "alice",
  age: 2,
};
var cat2 = {
  catName: "bob",
};

// tạo lớp đối tượng
// khuôn bánh
function Cat(_name, _age) {
  this.name = _name;
  this.age = _age;
  this.talk = function () {
    console.log("gâu gâu");
  };
}
// cat3 ~ bánh được tạo ra từ khuôn
var cat3 = new Cat("mun", 3);
// console.log("cat3: ", cat3);
// cat3.talk();
