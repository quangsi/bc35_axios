// pending, resolve, reject
// axios ~ bất đồng bộ
const BASE_URL = "https://633ec05b0dbc3309f3bc5455.mockapi.io";

var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

// lấy danh sách sinh viên service
var fetchDssvService = function () {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (response) {
      renderDanhSachSinhVien(response.data);
      tatLoading();
    })
    .catch(function (error) {
      tatLoading();
      console.log("error: ", error);
    });
};
// chạy lần đầu khi load trang
fetchDssvService();

// render danh sách sinh viên

var renderDanhSachSinhVien = function (listSv) {
  var contentHTML = "";
  listSv.forEach(function (sv) {
    contentHTML += `<tr>
                <td>${sv.ma}</td>
                <td>${sv.ten}</td>
                <td>${sv.email}</td>
                <td>0</td>
                <td>
                <button class="btn btn-primary">Sửa</button>
                <button onclick="xoaSv(${sv.ma})" class="btn btn-danger">Xoá</button>
                </td>
              </tr>`;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

// xoa sv

var xoaSv = function (idSv) {
 
  axios({
    url: `${BASE_URL}/sv/${idSv}`,
    method: "DELETE",
  })
    .then(function (res) {
      // gọi lại api lấy danh sách sau khi xoá thành công
      fetchDssvService();
      // thông báo thành công
      Swal.fire("Xoá thàn công");
      console.log("res: ", res);
    })
    .catch(function (err) {
      Swal.fire("Xoá thất bại");

      console.log("err: ", err);
    });
};

// them sinh vien

var themSv = function () {
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      Swal.fire("Thêm thàn công");
      fetchDssvService();
    })
    .catch(function (err) {
      Swal.fire("Thêm thất bại");
    });
};
